package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int rows;
	private int cols;
	private CellState state[][];

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.cols = columns;
		this.state = new CellState [rows] [cols];
		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				this.state[row][col] = initialState;
			}
		}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > rows) {
        	throw new IndexOutOfBoundsException("Row argument out of bounds");
        } else if (column < 0 || column > cols) {
        	throw new IndexOutOfBoundsException("Column argument out of bounds");
        } else {
        	state[row][column] = element;
        }
        
    }

    @Override
    public CellState get(int row, int column) {
    	if (row < 0 || row > rows) {
        	throw new IndexOutOfBoundsException("Row argument out of bounds");
        } else if (column < 0 || column > cols) {
        	throw new IndexOutOfBoundsException("Column argument out of bounds");
        } else {
        	return state[row][column];
        }
        
    }

    @Override
    public IGrid copy() {
    	CellGrid copy = new CellGrid(rows, cols, CellState.DEAD);
    	for (int row = 0; row<rows; row++) {
    		for (int col = 0; col<cols; col++) {
    			if (state[row][col].equals(CellState.ALIVE)) {
    				copy.set(row, col, CellState.ALIVE);
    			} else if (state[row][col].equals(CellState.DYING)) {
    				copy.set(row, col, CellState.DYING);
    			}
    		}
    	}
        return copy;
    }
    
}
